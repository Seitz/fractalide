@0xbc078cc6e946648c;

struct JsInput {
    label @0 :Text;
    disabled @1 :Bool;
    size @2 :UInt32 = 20;
    maxSize @3 :UInt32 = 524288;
    css @4 :Text;
    blockCss @5 :Text;
}

